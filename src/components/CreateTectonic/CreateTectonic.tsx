import { IconButton, makeStyles, Theme } from "@material-ui/core";
import { BorderClear, BorderInner, Delete, Filter1, Save } from "@material-ui/icons";
import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";

import { checkTectonicSimpleErrors } from "../../common/checkTectonicSimpleErrors";
import { createEmptyTectonic } from "../../common/createEmptyTectonic";
import { getDataURL } from "../../common/getDataURL";
import { getNonDiagonalNeighbors } from "../../common/getNonDiagonalNeighbors";
import { resetBlocks } from "../../common/resetBlocks";
import { setBlock } from "../../common/setBlock";
import { solver } from "../../common/solver";
import { clearValues, resetTectonic, setValuesLeft } from "../../common/tectonicUtils";
import { curryClamp } from "../../common/utils";
import { tectonicMaxBlockSize } from "../../constants/tectonic";
import TectonicCell from "../TectonicCell/TectonicCell";

interface TectonicProps {
  edit?: boolean;
  puzzleNumber: number;
  tectonic: Tectonic;
}

const useStyles = makeStyles<Theme, boolean>((theme) => ({
  root: {
    width: "fit-content",
    margin: "0 auto",
    paddingRight: theme.spacing(4),
  },
  topRow: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "space-between",
  },
  topLeftActions: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    height: theme.spacing(4),
    paddingLeft: theme.spacing(0.75),
  },
  topActions: {
    display: "flex",
    flexDirection: "row",
    flexGrow: 1,
    justifyContent: "flex-end",
    height: theme.spacing(4),
  },
  speedResult: {
    marginLeft: theme.spacing(1),
    lineHeight: `${theme.spacing(4)}px`,
    fontSize: "0.8125rem",
  },
  setShowOptionsSwitch: {
    marginTop: theme.spacing(0.4),
  },
  puzzleNumber: (isUnsolvable) => ({
    width: theme.spacing(5),
    height: theme.spacing(5),
    textAlign: "center",
    lineHeight: `${theme.spacing(5)}px`,
    fontWeight: "bold",
    backgroundColor: isUnsolvable ? theme.palette.error.main : theme.palette.primary.main,
    color:
      theme.palette.type === "light"
        ? theme.palette.background.default
        : theme.palette.text.primary,
    fontSize: "1.5rem",
  }),
  puzzleContainer: (isUnsolvable) => ({
    marginTop: -theme.spacing(1),
    marginLeft: theme.spacing(4),
    border: `${theme.spacing(1)}px solid ${
      isUnsolvable ? theme.palette.error.main : theme.palette.primary.main
    }`,
  }),
  puzzle: {
    display: "flex",
    flexDirection: "column",
  },
  row: {
    display: "flex",
  },
}));

enum EditModes {
  "BLOCKS",
  "CELLS",
}

const CreateTectonic: React.FC<TectonicProps> = ({ puzzleNumber }) => {
  const [tectonic, setTectonic] = useState(createEmptyTectonic());
  const classes = useStyles(tectonic.isUnsolvable);
  const [selectedCellIds, setSelectedCellIds] = useState(new Set<number>());
  const [errorCellIds, setErrorCellIds] = useState(new Set<number>());
  const [editMode, setEditMode] = useState<EditModes>(EditModes.BLOCKS);
  const [blockId, setBlockId] = useState(-1);
  const [hasBlocks, setHasBlocks] = useState(false);
  const [hasValues, setHasValues] = useState(false);
  const tectonicRef = useRef<HTMLDivElement>(null);

  const nonDiagonalNeighborCellIds = useMemo(() => {
    const neighborCellIds = new Set<number>();
    selectedCellIds.forEach((cellId) =>
      getNonDiagonalNeighbors(cellId, tectonic.size).forEach((neighborCellId) =>
        neighborCellIds.add(neighborCellId),
      ),
    );
    return neighborCellIds;
  }, [tectonic, selectedCellIds]);

  const tectonicPerRow: TectonicCell[][] = useMemo(() => {
    const tectonicPerRow: TectonicCell[][] = [];
    tectonic.cells.forEach((cell) => {
      if (tectonicPerRow[cell.rowId]) {
        tectonicPerRow[cell.rowId].push(cell);
      } else {
        tectonicPerRow[cell.rowId] = [cell];
      }
    });
    return tectonicPerRow;
  }, [tectonic]);

  const doneCreatingBlocks = useCallback(() => {
    resetBlocks(tectonic, true);
    return !tectonic.cells.some((cell) => cell.blockId === -1);
  }, [tectonic]);

  // Effects of switching editMode
  useEffect(() => {
    if (editMode === EditModes.BLOCKS) {
      setSelectedCellIds(new Set());
    } else if (editMode === EditModes.CELLS) {
      // Check if we can switch
      if (!doneCreatingBlocks()) {
        // Mark areas larger than 5 blocks red
        tectonic.cells
          .filter((cell) => cell.blockId === -1)
          .forEach((cell) => {
            setErrorCellIds((ids) => ids.add(cell.cellId));
          });
        // We cannot switch because there are unidentified blocks
        setEditMode(EditModes.BLOCKS);
        return;
      }
      setErrorCellIds(new Set());
    }
  }, [doneCreatingBlocks, editMode, tectonic]);

  const clear = useCallback(() => {
    setEditMode(EditModes.BLOCKS);
    setTectonic((t) => createEmptyTectonic(t.size));
    setBlockId(-1);
    setSelectedCellIds(new Set());
    setErrorCellIds(new Set());
    setHasBlocks(false);
  }, []);

  const clearAllValues = useCallback(() => {
    setTectonic((t) => clearValues(t, true));
  }, []);

  const onDragStart = useCallback(
    (startBlockId: number, startCellId: number) => () => {
      if (editMode === EditModes.BLOCKS) {
        setErrorCellIds(new Set());
        if (startBlockId === -1) {
          setSelectedCellIds(new Set([startCellId]));
          setBlockId(tectonic.blocks.length);
        } else {
          setSelectedCellIds(new Set([...tectonic.blocks[startBlockId].cellIds]));
          setBlockId(startBlockId);
        }
      }
    },
    [editMode, tectonic.blocks],
  );

  const onDragOver = useCallback(
    (cellId: number) => () => {
      // Skip if the dragOver cell is...
      if (
        selectedCellIds.has(cellId) || // Already in the list, or
        selectedCellIds.size >= tectonicMaxBlockSize || // The list is full (max size = 5), or
        !nonDiagonalNeighborCellIds.has(cellId) // Not a neighbor (non-diagonally)
      ) {
        return;
      }
      setSelectedCellIds((ids) => new Set(ids.add(cellId)));
    },
    [nonDiagonalNeighborCellIds, selectedCellIds],
  );

  const onDragEnd = useCallback(() => {
    setBlock(tectonic, blockId, selectedCellIds);
    setBlockId(-1);
    setErrorCellIds(new Set());
    setSelectedCellIds(new Set());
    setHasBlocks(true);
  }, [blockId, selectedCellIds, tectonic]);

  const onClick = useCallback(
    (cellId: number) => () => {
      if (editMode === EditModes.BLOCKS) {
        setErrorCellIds(new Set());
        if (
          tectonic.cells[cellId].blockId === -1 ||
          (tectonic.blocks[tectonic.cells[cellId].blockId] &&
            tectonic.blocks[tectonic.cells[cellId].blockId].size !== 1)
        ) {
          setBlock(tectonic, tectonic.blocks.length, new Set([cellId]));
          setBlockId(-1);
          setSelectedCellIds(new Set());
        }
      } else if (editMode === EditModes.CELLS) {
        const cellIdSet = new Set([cellId]);
        setSelectedCellIds(cellIdSet);
      }
    },
    [editMode, tectonic],
  );

  const clamp = useCallback(
    curryClamp({ min: 0, max: tectonic.size.width * tectonic.size.height - 1 }),
    [tectonic.size],
  );

  const moveCellId = useCallback(
    (direction: "up" | "right" | "down" | "left", add = false) => {
      setSelectedCellIds(
        (ids): Set<number> => {
          const current: number | undefined = Array.from(ids).pop();
          if (add && current !== undefined && ids.size === 1) {
            const blockId = tectonic.cells[current].blockId;
            setBlockId(blockId === -1 ? tectonic.blocks.length : blockId);
          }
          let desired = current;
          if (current === undefined) {
            desired = direction === "up" || direction === "left" ? tectonic.cells.length - 1 : 0;
          } else {
            if (direction === "left") {
              desired = clamp(current - 1, current);
            } else if (direction === "up") {
              desired = clamp(current - tectonic.size.width, current);
            } else if (direction === "right") {
              desired = clamp(current + 1, current);
            } else {
              // if (direction === "down") {
              desired = clamp(current + tectonic.size.width, current);
            }
          }
          if (add) {
            if (ids.has(desired)) {
              const idsArr = Array.from(ids);
              idsArr.pop(); // remove last item
              return new Set(idsArr);
            }
            if (ids.size >= tectonicMaxBlockSize || !nonDiagonalNeighborCellIds.has(desired)) {
              return ids;
            }
            return new Set(ids.add(desired));
          } else {
            return new Set([desired]);
          }
        },
      );
    },
    [
      clamp,
      nonDiagonalNeighborCellIds,
      tectonic.blocks.length,
      tectonic.cells,
      tectonic.size.width,
    ],
  );

  const onCtrlUp = useCallback(() => {
    const ids = Array.from(selectedCellIds);
    const lastId = ids[ids.length - 1];
    if (selectedCellIds.size === 1) {
      onClick(lastId)();
    } else {
      onDragEnd();
    }
    setSelectedCellIds(new Set([lastId]));
  }, [onClick, onDragEnd, selectedCellIds]);

  const setGivenValue = useCallback(
    (value?: number) => {
      if (value && editMode === EditModes.BLOCKS) {
        if (doneCreatingBlocks()) {
          setEditMode(EditModes.CELLS);
        } else {
          return;
        }
      }
      const selectedCellId = selectedCellIds.values().next().value;
      if (selectedCellId !== undefined) {
        if (value && value > tectonic.blocks[tectonic.cells[selectedCellId].blockId].size) {
          console.warn("value bigger than block size.. ignoring");
          return;
        }
        if (
          value &&
          tectonic.cells[selectedCellId].isGiven &&
          tectonic.cells[selectedCellId].value === value
        ) {
          value = undefined;
        }
        tectonic.cells[selectedCellId].value = value;
        tectonic.cells[selectedCellId].isGiven = value !== undefined;
        tectonic.cells[selectedCellId].options = [];
        setValuesLeft(tectonic);
        setTectonic({ ...tectonic });
      }
    },
    [doneCreatingBlocks, editMode, selectedCellIds, tectonic],
  );

  useEffect(() => {
    const keyDownFunction = (e: KeyboardEvent) => {
      // console.log(e.keyCode);
      switch (e.keyCode) {
        case 37: // ArrowLeft
          moveCellId("left", e.ctrlKey);
          break;
        case 38: // ArrowUp
          moveCellId("up", e.ctrlKey);
          break;
        case 39: // ArrowRight
          moveCellId("right", e.ctrlKey);
          break;
        case 40: // ArrowDown
          moveCellId("down", e.ctrlKey);
          break;
        case 49: // 1
          setGivenValue(1);
          break;
        case 50: // 2
          setGivenValue(2);
          break;
        case 51: // 3
          setGivenValue(3);
          break;
        case 52: // 4
          setGivenValue(4);
          break;
        case 53: // 5
          setGivenValue(5);
          break;
        case 27: // Esc
          setSelectedCellIds(new Set());
          break;
        case 8: // Backspace
          setGivenValue();
          break;
        case 46: // Delete
          setGivenValue();
          break;
        case 48: // 0
          setGivenValue();
          break;
      }
    };
    const keyUpFunction = (e: KeyboardEvent) => {
      if (e.keyCode === 17) {
        onCtrlUp();
      }
    };
    document.addEventListener("keydown", keyDownFunction);
    document.addEventListener("keyup", keyUpFunction);
    return () => {
      document.removeEventListener("keydown", keyDownFunction);
      document.removeEventListener("keyup", keyUpFunction);
    };
  }, [setGivenValue, moveCellId, onCtrlUp, onDragEnd]);

  const save = useCallback(async () => {
    if (tectonicRef.current) {
      setSelectedCellIds(new Set());
      setEditMode(EditModes.CELLS);
      setTimeout(async () => {
        if (tectonicRef.current && editMode === EditModes.CELLS) {
          const dataURL = await getDataURL(tectonicRef.current, { maxWidth: 120, maxHeight: 120 });
          const scaledImage = new Image();
          scaledImage.src = dataURL;
          scaledImage.style.marginLeft = "10px";
          document.body.appendChild(scaledImage);
        }
      }, 500);
    }
  }, [editMode]);

  // Effects after switching modes or changing a value
  useEffect(() => {
    setHasValues(tectonic.cells.some((cell) => cell.value !== undefined));
    const hasErrors = checkTectonicSimpleErrors(tectonic);
    if (hasErrors) {
      setTectonic({ ...tectonic });
    }
    if (editMode === EditModes.CELLS) {
      resetTectonic(tectonic);
      if (!hasErrors && !tectonic.cells.some((cell) => cell.isError || cell.blockId === -1)) {
        console.groupCollapsed("Running solver");
        console.log("before: ", tectonic);
        solver(tectonic, { pauze: false });
        console.log("after", tectonic);
        console.groupEnd();
      }
    }
  }, [tectonic, editMode]);

  return (
    <div className={classes.root}>
      <div className={classes.topRow}>
        <div className={classes.puzzleNumber}>{puzzleNumber}</div>
        <div className={classes.topLeftActions}>
          <IconButton size="small" onClick={() => setEditMode(EditModes.BLOCKS)}>
            <BorderInner color={editMode === EditModes.BLOCKS ? "primary" : undefined} />
          </IconButton>
          <IconButton size="small" onClick={() => setEditMode(EditModes.CELLS)}>
            <Filter1 color={editMode === EditModes.CELLS ? "primary" : undefined} />
          </IconButton>
        </div>
        <div className={classes.topActions}>
          <IconButton disabled={!hasBlocks} size="small" onClick={clear}>
            <BorderClear />
          </IconButton>
          <IconButton disabled={!hasValues} size="small" onClick={clearAllValues}>
            <Delete />
          </IconButton>
          <IconButton size="small" onClick={save}>
            <Save />
          </IconButton>
        </div>
      </div>
      <div className={classes.puzzleContainer}>
        <div className={classes.puzzle} ref={tectonicRef} onDragEnd={onDragEnd}>
          {tectonicPerRow.map((row, rowId) => (
            <div className={classes.row} key={`tectonic-${puzzleNumber}-${rowId}`}>
              {row.map((cell) => (
                <TectonicCell
                  blockId={
                    editMode === EditModes.CELLS || cell.blockId === -1 ? undefined : cell.blockId
                  }
                  blockSize={
                    tectonic.blocks[cell.blockId] ? tectonic.blocks[cell.blockId].size : -1
                  }
                  cell={cell}
                  hideNonGiven={editMode === EditModes.BLOCKS}
                  isError={errorCellIds.has(cell.cellId)}
                  isMaxSelected={
                    selectedCellIds.size === tectonicMaxBlockSize ||
                    errorCellIds.size === tectonicMaxBlockSize
                  }
                  isSelected={selectedCellIds.has(cell.cellId)}
                  key={`tectonic-${puzzleNumber}-${rowId}-${cell.cellId}`}
                  onClick={onClick}
                  onDragOver={editMode === EditModes.BLOCKS ? onDragOver : undefined}
                  onDragStart={editMode === EditModes.BLOCKS ? onDragStart : undefined}
                />
              ))}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default CreateTectonic;

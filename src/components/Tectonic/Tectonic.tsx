import { IconButton, makeStyles, Switch, Theme, Typography } from "@material-ui/core";
import { CheckBox, SkipNext } from "@material-ui/icons";
import React, { useCallback, useMemo, useState } from "react";

import { solver } from "../../common/solver";
import TectonicCell from "../TectonicCell/TectonicCell";

interface TectonicProps {
  edit?: boolean;
  puzzleNumber: number;
  tectonic: Tectonic;
}

const useStyles = makeStyles<Theme, boolean>((theme) => ({
  root: {
    width: "fit-content",
    margin: "0 auto",
    paddingRight: theme.spacing(4),
  },
  topRow: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "space-between",
  },
  topActions: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    height: theme.spacing(4),
  },
  speedResult: {
    marginLeft: theme.spacing(1),
    lineHeight: `${theme.spacing(4)}px`,
    fontSize: "0.8125rem",
  },
  setShowOptionsSwitch: {
    marginTop: theme.spacing(0.4),
  },
  puzzleNumber: (isUnsolvable) => ({
    width: theme.spacing(5),
    height: theme.spacing(5),
    textAlign: "center",
    lineHeight: `${theme.spacing(5)}px`,
    fontWeight: "bold",
    backgroundColor: isUnsolvable ? theme.palette.error.main : theme.palette.primary.main,
    color:
      theme.palette.type === "light"
        ? theme.palette.background.default
        : theme.palette.text.primary,
    fontSize: "1.5rem",
  }),
  puzzle: (isUnsolvable) => ({
    marginTop: -theme.spacing(1),
    marginLeft: theme.spacing(4),
    border: `${theme.spacing(1)}px solid ${
      isUnsolvable ? theme.palette.error.main : theme.palette.primary.main
    }`,
    display: "flex",
    flexDirection: "column",
  }),
  row: {
    display: "flex",
  },
}));

const Tectonic: React.FC<TectonicProps> = ({ tectonic: tectonicProp, puzzleNumber }) => {
  const [tectonic, setTectonic] = useState(tectonicProp);
  const classes = useStyles(tectonic.isUnsolvable);
  const [showOptions, setShowOptions] = useState(true);
  const [selectedCellId, setSelectedCellId] = useState(-1);

  const tectonicPerRow: TectonicCell[][] = useMemo(() => {
    const tectonicPerRow: TectonicCell[][] = [];
    tectonic.cells.forEach((cell) => {
      if (tectonicPerRow[cell.rowId]) {
        tectonicPerRow[cell.rowId].push(cell);
      } else {
        tectonicPerRow[cell.rowId] = [cell];
      }
    });
    return tectonicPerRow;
  }, [tectonic]);

  const solve = useCallback(
    (pauze = false) => {
      setTectonic((t) => Object.assign({}, solver(t, { pauze })));
    },
    [setTectonic],
  );

  const onClick = useCallback(
    (cellId: number) => () => {
      setSelectedCellId((id) => (id === cellId ? -1 : cellId));
    },
    [],
  );

  return (
    <div className={classes.root}>
      <div className={classes.topRow}>
        <div className={classes.puzzleNumber}>{puzzleNumber}</div>
        <div className={classes.topActions}>
          {!tectonic.isUnsolvable && tectonic.nUnsolvedCells > 0 && (
            <>
              <IconButton size="small" onClick={() => solve(true)}>
                <SkipNext />
              </IconButton>
              <IconButton size="small" onClick={() => solve(false)}>
                <CheckBox />
              </IconButton>
            </>
          )}
          <Switch
            checked={showOptions}
            className={classes.setShowOptionsSwitch}
            color="primary"
            disabled={!tectonic.trackOptions}
            inputProps={{ "aria-label": "options" }}
            name="showOptions"
            size="small"
            onChange={() => setShowOptions((o) => !o)}
          />
        </div>
      </div>
      <div className={classes.puzzle}>
        {tectonicPerRow.map((row, rowId) => (
          <div className={classes.row} key={`tectonic-${puzzleNumber}-${rowId}`}>
            {row.map((cell) => (
              <TectonicCell
                blockSize={tectonic.blocks[cell.blockId].size}
                cell={cell}
                isSelected={selectedCellId === cell.cellId}
                key={`tectonic-${puzzleNumber}-${rowId}-${cell.cellId}`}
                showOptions={showOptions}
                onClick={onClick}
              />
            ))}
          </div>
        ))}
      </div>
      {tectonic.start && tectonic.finish && (
        <Typography className={classes.speedResult}>
          {(tectonic.finish - tectonic.start).toFixed(2)} ms.
        </Typography>
      )}
      {selectedCellId !== -1 && (
        <Typography className={classes.speedResult}>selected cellId: {selectedCellId}</Typography>
      )}
    </div>
  );
};

export default Tectonic;

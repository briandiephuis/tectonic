import { makeStyles, Theme } from "@material-ui/core";
import { InsertLink, Looks5 } from "@material-ui/icons";
import React from "react";

interface TectonicCellProps {
  cell: TectonicCell;
  blockId?: number;
  blockSize: number;
  hideNonGiven?: boolean;
  showOptions?: boolean;
  isSelected: boolean;
  isMaxSelected?: boolean;
  isError?: boolean;
  onClick?: (cellId: number) => (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  onDragStart?: (
    blockId: number,
    cellId: number,
  ) => (event: React.DragEvent<HTMLDivElement>) => void;
  onDragOver?: (cellId: number) => (event: React.DragEvent<HTMLDivElement>) => void;
}

interface TectonicCellStyleProps {
  cell: TectonicCell;
  isSelected: boolean;
  isError: boolean;
  blockSize: number;
}

const useStyles = makeStyles<Theme, TectonicCellStyleProps>((theme) => {
  const size = theme.spacing(6);
  const wallThickness = 1;
  const fatWallThickness = 3;
  const offset = wallThickness / 2;
  const fatOffset = fatWallThickness / 2;

  return {
    root: ({ cell: { walls, isError: cellIsError }, isError, isSelected, blockSize }) => {
      const width = size + (walls.left ? fatOffset : offset) + (walls.right ? fatOffset : offset);
      const height = size + (walls.top ? fatOffset : offset) + (walls.bottom ? fatOffset : offset);
      const regularBorder = `${wallThickness}px solid ${theme.palette.text.secondary}`;
      const fatBorder = `${fatWallThickness}px solid ${theme.palette.text.primary}`;
      return {
        flex: 1,
        width,
        minWidth: width,
        maxWidth: width,
        height,
        marginTop: walls.top ? -fatOffset : -offset,
        marginRight: walls.right ? -fatOffset : -offset,
        marginBottom: walls.bottom ? -fatOffset : -offset,
        marginLeft: walls.left ? -fatOffset : -offset,
        borderTop: walls.top ? fatBorder : regularBorder,
        borderRight: walls.right ? fatBorder : regularBorder,
        borderBottom: walls.bottom ? fatBorder : regularBorder,
        borderLeft: walls.left ? fatBorder : regularBorder,
        fontSize: "2rem",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        flexWrap: "wrap",
        alignContent: "center",
        position: "relative",
        "&:before": {
          content: '""',
          position: "absolute",
          top: 0,
          left: 0,
          zIndex: -1,
          width: "100%",
          height: "100%",
          opacity: 0.8,
          backgroundColor: isError
            ? theme.palette.error.light
            : isSelected
            ? theme.palette.secondary.light
            : blockSize < 0
            ? theme.palette.background.paper
            : "inherit",
        },
        "&:hover": {
          "&:before": {
            opacity: isSelected ? 1 : 0.5,
            backgroundColor: isError ? theme.palette.error.main : theme.palette.secondary.light,
          },
        },
      };
    },
    value: ({ cell: { isGiven, isError } }) => ({
      opacity: 1,
      zIndex: 1,
      lineHeight: `${size}px`,
      color: isError ? theme.palette.error.dark : theme.palette.text.primary,
      fontWeight: isGiven ? 700 : 300, // 700 = bold, 300 = light
      textAlign: "center",
      userSelect: "none",
      marginTop: 2,
    }),
    blockIdContainer: {
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    blockId: {
      userSelect: "none",
      fontSize: "0.8rem",
      fontWeight: "bold",
      lineHeight: `${theme.spacing(3) + 1}px`,
      color: theme.palette.background.default,
      textAlign: "center",
      backgroundColor: theme.palette.background.paper,
      width: theme.spacing(3),
      height: theme.spacing(3),
      borderRadius: "50%",
    },
    option: {
      fontSize: "0.8rem",
      color: theme.palette.grey[500],
      display: "block",
      width: theme.spacing(1.5),
      minWidth: theme.spacing(1.5),
      height: theme.spacing(2),
      textAlign: "center",
      userSelect: "none",
    },
    draggable: {
      color:
        theme.palette.type === "dark"
          ? theme.palette.background.default
          : theme.palette.text.secondary,
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      border: "none",
      textAlign: "center",
    },
  };
});

const TectonicCell: React.FunctionComponent<TectonicCellProps> = ({
  blockId,
  blockSize,
  cell,
  hideNonGiven = false,
  showOptions = true,
  isError = false,
  isSelected,
  isMaxSelected = false,
  onClick,
  onDragOver,
  onDragStart,
}) => {
  const classes = useStyles({ cell, isError, isSelected, blockSize });

  return (
    <div className={classes.root} onClick={onClick && onClick(cell.cellId)}>
      {blockId !== undefined && !isSelected && (
        <div className={classes.blockIdContainer}>
          <div className={classes.blockId}>{blockId + 1}</div>
        </div>
      )}
      {cell.value && (!hideNonGiven || cell.isGiven) && (
        <div className={classes.value}>{cell.value}</div>
      )}
      {!cell.value &&
        !hideNonGiven &&
        showOptions &&
        blockSize >= 0 &&
        new Array(blockSize).fill(0).map((_, i) => (
          <div className={classes.option} key={`${cell.cellId}-${i + 1}`}>
            {cell.options.some((option) => option === i + 1) && i + 1}
          </div>
        ))}
      {onDragStart !== undefined && (
        <div
          className={classes.draggable}
          draggable={true}
          onDragOver={onDragOver && onDragOver(cell.cellId)}
          onDragStart={onDragStart && onDragStart(cell.blockId, cell.cellId)}
        >
          {isMaxSelected && (isSelected || isError) && <Looks5 />}
          {!isMaxSelected && isSelected && <InsertLink />}
        </div>
      )}
    </div>
  );
};

export default TectonicCell;

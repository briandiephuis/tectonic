import {
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader,
  makeStyles,
  SwipeableDrawer,
  Typography,
  useMediaQuery,
} from "@material-ui/core";
import { Brightness4, LibraryAdd, LibraryBooks, MenuOpen, Shuffle } from "@material-ui/icons";
import React, { useCallback, useContext } from "react";
import { Link } from "react-router-dom";

import { UserSettings } from "../../common/UserSettings";

const useStyles = makeStyles((theme) => ({
  button: {
    position: "fixed",
    top: theme.spacing(1),
    right: theme.spacing(1),
  },
  drawer: {
    width: theme.spacing(34),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    height: "100%",
  },
  logo: {
    marginLeft: theme.spacing(2),
    fontSize: "2rem",
    lineHeight: "2rem",
  },
  menu: {
    flexGrow: 1,
  },
  list: {
    //
  },
}));

const Drawer: React.FC = () => {
  const classes = useStyles();
  const [userSettings, setUserSettings] = useContext(UserSettings);
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");

  const toggleDrawer = useCallback(() => {
    setUserSettings((s) => ({ ...s, isDrawerOpen: !s.isDrawerOpen }));
  }, [setUserSettings]);

  const toggleDarkMode = useCallback(() => {
    setUserSettings((s) => ({
      ...s,
      darkMode: s.darkMode === undefined ? true : s.darkMode ? false : undefined,
    }));
  }, [setUserSettings]);

  return (
    <>
      <div className={classes.button}>
        <IconButton onClick={toggleDrawer}>
          <MenuOpen />
        </IconButton>
      </div>
      <SwipeableDrawer
        anchor="right"
        open={userSettings.isDrawerOpen}
        onClose={toggleDrawer}
        onOpen={toggleDrawer}
      >
        <div className={classes.drawer}>
          <Typography className={classes.logo}>Tectonic</Typography>
          <List aria-labelledby="menu" className={classes.menu}>
            <ListItem button component={Link} to="/">
              <ListItemIcon>
                <LibraryBooks />
              </ListItemIcon>
              <ListItemText primary="Puzzles" />
            </ListItem>
            <ListItem button component={Link} to="/random">
              <ListItemIcon>
                <Shuffle />
              </ListItemIcon>
              <ListItemText primary="Random puzzle" />
            </ListItem>
            <ListItem button component={Link} to="/create">
              <ListItemIcon>
                <LibraryAdd />
              </ListItemIcon>
              <ListItemText primary="Create a puzzle" />
            </ListItem>
          </List>
          <List
            aria-labelledby="settings"
            className={classes.list}
            subheader={
              <ListSubheader component="div" id="settings-subheader">
                Settings
              </ListSubheader>
            }
          >
            <ListItem button onClick={toggleDarkMode}>
              <ListItemIcon>
                <Brightness4 />
              </ListItemIcon>
              <ListItemText
                primary="Light/dark theme"
                secondary={`Current: ${
                  userSettings.darkMode === undefined
                    ? `OS/Browser (${prefersDarkMode ? "dark" : "light"})`
                    : userSettings.darkMode
                    ? "dark"
                    : "light"
                }`}
              />
            </ListItem>
          </List>
        </div>
      </SwipeableDrawer>
    </>
  );
};

export default Drawer;

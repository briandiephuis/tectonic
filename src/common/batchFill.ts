import { solver } from "./solver";
import { setOptions } from "./tectonicUtils";

export const batchFill = (
  tectonic: Tectonic,
  answers: TectonicAnswer[],
  options?: SolverOptions,
): Tectonic => {
  tectonic.iteration++;

  answers.forEach((answer) => {
    if (tectonic.cells[answer.cellId].value) {
      // Answer already filled. Clear options on this cell.
      tectonic.cells[answer.cellId].options = [];
      console.error("Answer already filled", { answer });
      return;
    }

    // Set answer
    tectonic.cells[answer.cellId].value = answer.value;
    tectonic.nUnsolvedCells--;

    // Remove answer from block's valuesLeft array
    const blockId = tectonic.cells[answer.cellId].blockId;
    tectonic.blocks[blockId].valuesLeft = tectonic.blocks[blockId].valuesLeft.filter(
      (value) => value !== answer.value,
    );

    // If we're at the point that we have to track the options of each cell,
    // then update those of the neighbors of this answer
    if (tectonic.trackOptions) {
      tectonic.cells[answer.cellId].neighbors.forEach((neighborId) => {
        if (tectonic.cells[neighborId].options.length > 0) {
          tectonic.cells[neighborId].options = tectonic.cells[neighborId].options.filter(
            (option) => option !== answer.value,
          );
        }
      });
      setOptions(tectonic);
    }
  });

  // Check if the puzzle is now solved
  if (tectonic.nUnsolvedCells > 0 && !options?.pauze) {
    // Another iteration needed
    return solver(tectonic, options);
  }

  tectonic.finish = performance.now();
  return tectonic;
};

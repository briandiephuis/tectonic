import { createContext } from "react";

export const initialUserSettings: UserSettings = {
  isDrawerOpen: false,
  darkMode: undefined, // Undefined to listen to the browser/OS setting
};

export const UserSettings = createContext<
  [UserSettings, React.Dispatch<React.SetStateAction<UserSettings>>]
>([
  initialUserSettings,
  () => {
    // can be empty, will be set by useState()
  },
]);

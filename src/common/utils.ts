interface ClampOptions {
  min?: number;
  max?: number;
}

export const curryClamp = ({ min, max }: ClampOptions) => (n: number, alternative?: number) => {
  if (min !== undefined && n < min) {
    return alternative === undefined ? min : alternative;
  } else if (max !== undefined && n > max) {
    return alternative === undefined ? max : alternative;
  }
  return n;
};

export const sequence = (n: number, start = 0) => new Array(n).fill(1).map((_, i) => i + start);

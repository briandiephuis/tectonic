export const getNonDiagonalNeighbors = (cellId: number, size: TectonicSize): number[] => {
  const row = Math.floor(cellId / size.width);
  const column = cellId % size.width;
  const nonDiagonalNeighbors: number[] = [];
  if (row !== 0) {
    nonDiagonalNeighbors.push(cellId - size.width); // North
  }
  if (column !== size.width - 1) {
    nonDiagonalNeighbors.push(cellId + 1); // East
  }
  if (row !== size.height - 1) {
    nonDiagonalNeighbors.push(cellId + size.width); // South
  }
  if (column !== 0) {
    nonDiagonalNeighbors.push(cellId - 1); // West
  }
  return nonDiagonalNeighbors;
};

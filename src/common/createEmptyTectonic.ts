import { newTectonic, noWalls } from "../constants/tectonic";
import { getNeighbors } from "./getNeighbors";
import { getWalls } from "./tectonicUtils";

export const createEmptyTectonic = (size: TectonicSize = newTectonic.size): Tectonic => {
  const nUnsolvedCells = size.width * size.height;
  const cells: TectonicCell[] = [];
  const blocks: TectonicBlock[] = [];

  for (let rowId = 0; rowId < size.height; rowId++) {
    for (let columnId = 0; columnId < size.width; columnId++) {
      const cellId = rowId * size.width + columnId;
      const neighbors = getNeighbors(cellId, size, rowId, columnId);
      cells.push({
        cellId: rowId * size.width + columnId,
        blockId: -1,
        columnId,
        rowId,
        value: undefined,
        isGiven: false,
        neighbors,
        options: [],
        blockNeighbors: [...neighbors],
        nonBlockNeighbors: [],
        nonBlockNeighborsPerBlock: {},
        isError: false,
        walls: noWalls,
      });
    }
  }

  cells.forEach((cell) => {
    cell.walls = getWalls(cell, size);
  });

  return {
    level: 1,
    size,
    blocks,
    cells,
    nUnsolvedCells,
    iteration: 0,
    isUnsolvable: false,
    trackOptions: false,
  };
};

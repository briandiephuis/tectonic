import { tectonicMaxBlockSize } from "../constants/tectonic";
import { getNonDiagonalNeighbors } from "./getNonDiagonalNeighbors";
import { getWalls } from "./tectonicUtils";

interface NumberDict {
  [key: number]: number[];
}

const floodFillGetIds = (
  tectonic: Tectonic,
  neighbors: NumberDict,
  blockId: number,
  cellId: number,
  ids?: Set<number>,
): Set<number> | undefined => {
  if (!ids) {
    ids = new Set();
  }
  if (tectonic.cells[cellId].blockId !== blockId || ids.has(cellId)) {
    return;
  }
  ids.add(cellId);

  neighbors[cellId]
    .filter((neighborId) => !ids?.has(neighborId))
    .forEach((neighborId) => {
      floodFillGetIds(tectonic, neighbors, blockId, neighborId, ids);
    });

  return ids.size <= tectonicMaxBlockSize ? ids : undefined;
};

export const resetBlocks = (tectonic: Tectonic, shouldTryToAssign = false): void => {
  // Create dictionary with neighbors
  const neighbors: NumberDict = {};
  tectonic.cells.forEach((cell) => {
    neighbors[cell.cellId] = getNonDiagonalNeighbors(cell.cellId, tectonic.size);
  });

  // Assign split blocks their own blockId
  const assignedCells = tectonic.cells.filter((cell) => shouldTryToAssign || cell.blockId !== -1);
  const unhandledCellIds = new Set(assignedCells.map((cell) => cell.cellId));
  const blocks: TectonicBlock[] = [];
  assignedCells.forEach((cell) => {
    // Skip if already handled
    if (!unhandledCellIds.has(cell.cellId)) {
      return;
    }
    const connectedSameBlockIds = floodFillGetIds(tectonic, neighbors, cell.blockId, cell.cellId);
    if (!connectedSameBlockIds) {
      return;
    }
    connectedSameBlockIds.forEach((id) => unhandledCellIds.delete(id));

    blocks.push({
      blockId: blocks.length,
      cellIds: Array.from(connectedSameBlockIds),
      size: connectedSameBlockIds.size,
      valuesLeft: [],
    });
  });

  // (3) Update blocks
  tectonic.blocks = blocks;

  // (4) Set cells to new blockIds
  blocks.forEach((block) =>
    block.cellIds.forEach((cellId) => {
      tectonic.cells[cellId].blockId = block.blockId;
    }),
  );

  // (5) Update other cell metadata
  tectonic.cells.forEach((cell) => {
    cell.blockNeighbors = cell.neighbors.filter(
      (neighborCellId) => tectonic.cells[neighborCellId].blockId === cell.blockId,
    );
    cell.nonBlockNeighbors = cell.neighbors.filter(
      (neighborCellId) => tectonic.cells[neighborCellId].blockId !== cell.blockId,
    );
    cell.walls = getWalls(cell, tectonic.size);
    cell.nonBlockNeighborsPerBlock = {};
    cell.nonBlockNeighbors.forEach((neighborId) => {
      // Create a dictionary grouping the nonBlockNeighbors per block by blockId
      const { blockId } = tectonic.cells[neighborId];
      if (cell.nonBlockNeighborsPerBlock[blockId]) {
        cell.nonBlockNeighborsPerBlock[blockId].push(neighborId);
      } else {
        cell.nonBlockNeighborsPerBlock[blockId] = [neighborId];
      }
    });
  });
};

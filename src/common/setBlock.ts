import { tectonicMaxBlockSize } from "../constants/tectonic";
import { resetBlocks } from "./resetBlocks";
import { sequence } from "./utils";

export const setBlock = (tectonic: Tectonic, newBlockId: number, newCellIds: Set<number>): void => {
  if (newCellIds.size > tectonicMaxBlockSize) {
    console.log("too big");
    return;
  }

  // Create or update new block
  const index = tectonic.blocks.findIndex((block) => block.blockId === newBlockId);
  const newBlock: TectonicBlock = {
    blockId: newBlockId,
    cellIds: Array.from(newCellIds),
    size: newCellIds.size,
    valuesLeft: sequence(newCellIds.size, 1),
  };
  if (index === -1) {
    tectonic.blocks.push(newBlock);
  } else {
    tectonic.blocks[index] = newBlock;
  }

  // Update blockIds in cells
  const newCells = tectonic.cells.filter(({ cellId }) => newCellIds.has(cellId));
  newCells.forEach((cell) => {
    if (newCellIds.has(cell.cellId)) {
      cell.blockId = newBlockId;
    }
  });

  // Update blocks (reindex and fix potentially split blocks)
  resetBlocks(tectonic);
};

import htmlToImage from "html-to-image";

import { curryClamp } from "./utils";

interface GetDataURLOptions {
  maxWidth: number;
  maxHeight: number;
}

const toDataURL = (canvas: HTMLCanvasElement) => canvas.toDataURL("image/webp", 0.4);

const filter = (domNode: HTMLElement): boolean => domNode.draggable === false;

export const getDataURL = async (
  ref: HTMLElement | HTMLDivElement,
  { maxWidth, maxHeight }: GetDataURLOptions,
): Promise<string> => {
  const canvas = await htmlToImage.toCanvas(ref, { filter });
  maxWidth = curryClamp({ min: 1, max: canvas.width })(maxWidth);
  maxHeight = curryClamp({ min: 1, max: canvas.height })(maxHeight);

  if (maxHeight === canvas.height && maxWidth === canvas.width) {
    return toDataURL(canvas);
  }

  const widthScale = maxWidth / canvas.width;
  const heightScale = maxHeight / canvas.height;
  const hasWidthLimit = widthScale < heightScale;
  const scale = Math.min(widthScale, heightScale);
  const scaledCanvas = document.createElement("canvas");
  const ctx = scaledCanvas.getContext("2d");
  if (!ctx) {
    throw new Error("Could not get canvas context");
  }
  scaledCanvas.width = hasWidthLimit
    ? Math.round(canvas.width * scale)
    : Math.ceil(canvas.width * scale);
  scaledCanvas.height = !hasWidthLimit
    ? Math.round(canvas.height * scale)
    : Math.ceil(canvas.height * scale);

  ctx.drawImage(canvas, 0, 0, Math.floor(canvas.width * scale), Math.floor(canvas.height * scale));
  return toDataURL(scaledCanvas);
};

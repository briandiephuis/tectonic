export const getNeighbors = (
  cellId: number,
  { width, height }: TectonicSize,
  rowId: number,
  columnId: number,
): number[] => {
  const neighbors: number[] = [];

  if (rowId !== 0) {
    // There are cells to the north of this one
    if (columnId !== 0) {
      // North and east
      neighbors.push(cellId - width - 1); // NE
    }
    neighbors.push(cellId - width); // N
    if (columnId !== width - 1) {
      // North and west
      neighbors.push(cellId - width + 1); // NW
    }
  }
  if (columnId !== 0) {
    // There are cells to the west of this one
    neighbors.push(cellId - 1); // W
  }
  if (columnId !== width - 1) {
    // There are cells to the east of this one
    neighbors.push(cellId + 1); // E
  }
  if (rowId !== height - 1) {
    // There are cells to the south of this one
    if (columnId !== 0) {
      // North and east
      neighbors.push(cellId + width - 1); // SE
    }
    neighbors.push(cellId + width); // S
    if (columnId !== width - 1) {
      // North and west
      neighbors.push(cellId + width + 1); // SW
    }
  }
  return neighbors;
};

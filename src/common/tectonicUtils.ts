import { sequence } from "./utils";

export const setValuesLeft = (tectonic: Tectonic): Tectonic => {
  tectonic.blocks.forEach((block) => {
    const valuesLeftSet = new Set(sequence(block.size, 1));
    block.cellIds.forEach((cellId) => {
      const value = tectonic.cells[cellId].value;
      if (value) {
        valuesLeftSet.delete(value);
      }
    });
    block.valuesLeft = Array.from(valuesLeftSet);
  });
  return tectonic;
};

export const clearValues = (tectonic: Tectonic, shouldClearGiven = false): Tectonic => {
  tectonic.cells.forEach((cell) => {
    if (!shouldClearGiven && !cell.isGiven) {
      cell.value = undefined;
      cell.options = [];
    }
  });

  return tectonic;
};

export const resetTectonic = (tectonic: Tectonic, shouldClearGiven = false): Tectonic => {
  tectonic.start = undefined;
  tectonic.finish = undefined;
  tectonic.iteration = 0;
  tectonic.isUnsolvable = false;
  tectonic.level = 0;
  tectonic.trackOptions = false;
  tectonic.nUnsolvedCells =
    tectonic.cells.length -
    tectonic.cells.reduce((sum, cell) => {
      if (cell.isGiven && cell.value !== undefined) {
        return sum + 1;
      }
      return sum;
    }, 0);
  clearValues(tectonic, shouldClearGiven);
  setValuesLeft(tectonic);

  return tectonic;
};

export const getWalls = (
  { cellId, rowId, columnId, blockNeighbors }: TectonicCell,
  { width, height }: TectonicSize,
): TectonicWalls => {
  return {
    top: rowId === 0 || !blockNeighbors.some((id) => id === cellId - width),
    right: columnId === width - 1 || !blockNeighbors.some((id) => id === cellId + 1),
    bottom: rowId === height - 1 || !blockNeighbors.some((id) => id === cellId + width),
    left: columnId === 0 || !blockNeighbors.some((id) => id === cellId - 1),
  };
};

export const setOptions = (tectonic: Tectonic): void => {
  tectonic.cells.forEach((cell) => {
    // Clear options when a value has been set
    if (cell.value !== undefined && cell.options.length !== 0) {
      cell.options = [];
    }

    // Get options by taking the values left in the block
    // and reducing those by the non-block neighbor values
    if (cell.value === undefined) {
      cell.options = tectonic.blocks[cell.blockId].valuesLeft.filter(
        (value) =>
          !cell.nonBlockNeighbors.some((neighborId) => tectonic.cells[neighborId].value === value),
      );
    }
  });
};

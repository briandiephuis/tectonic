import { batchFill } from "./batchFill";
import { setOptions } from "./tectonicUtils";

const MAX_ITERATIONS = 100;

export const solver = (tectonic: Tectonic, options?: SolverOptions): Tectonic => {
  const answers: TectonicAnswer[] = [];

  if (tectonic.isUnsolvable) {
    // eslint-disable-next-line no-console
    console.error("Cannot solve, already marked unsolvable");
    return tectonic;
  }

  if (
    tectonic.iteration > MAX_ITERATIONS ||
    (options?.maxIterations && tectonic.iteration > options.maxIterations)
  ) {
    // eslint-disable-next-line no-console
    console.error("Max iterations reached");
    return tectonic;
  }

  // (3+) if we track options, check if there are cells with just one option, then fill that option
  if (tectonic.trackOptions) {
    tectonic.cells
      .filter((cell) => cell.options.length === 1)
      .forEach(({ cellId, options }) => {
        answers.push({ cellId, value: options[0] });
      });
  }

  if (tectonic.iteration === 0) {
    tectonic.start = performance.now();
  }

  // (1) Find all block with one value left, enter that value
  // also: all blocks of size 1 will be filled on the first iteration
  tectonic.blocks
    .filter((block) => block.valuesLeft.length === 1)
    .forEach((block) => {
      if (block.size === 1) {
        answers.push({ cellId: block.cellIds[0], value: block.valuesLeft[0] });
      } else {
        const cellId = block.cellIds.find((cellId) => tectonic.cells[cellId].value === undefined);
        if (cellId !== undefined) {
          answers.push({ cellId, value: block.valuesLeft[0] });
        }
      }
    });

  if (answers.length !== 0) {
    return batchFill(tectonic, answers, options);
  }

  /**
   * LEVEL 2
   */
  if (tectonic.level < 2) {
    tectonic.level = 2;
  }

  // (2.1) Within a block, try to place the remaining values
  tectonic.blocks.forEach((block) => {
    // Skip if this block has been solved
    if (block.valuesLeft.length === 0) {
      return;
    }

    const freeCells = block.cellIds
      .map((id) => tectonic.cells[id])
      .filter((cell) => cell.value === undefined);

    if (freeCells.length === 1) {
      answers.push({
        cellId: freeCells[0].cellId,
        value: block.valuesLeft[0],
      });
      return;
    }

    block.valuesLeft.forEach((value) => {
      let nPossibleCells = freeCells.length;
      let possibleCellId = -1;
      freeCells.forEach((cell) => {
        // Can we place the cell here?
        if (
          cell.nonBlockNeighbors.some((neighborId) => tectonic.cells[neighborId].value === value)
        ) {
          nPossibleCells--;
        } else {
          possibleCellId = cell.cellId;
        }
      });
      if (nPossibleCells === 1) {
        // There's one option left, place it there
        answers.push({ cellId: possibleCellId, value });
      }
    });
  });

  if (answers.length !== 0) {
    return batchFill(tectonic, answers, options);
  }

  // (2.2) Loop over empty cells in a block and try the different values left for that block
  tectonic.blocks.forEach((block) => {
    block.cellIds.forEach((cellId) => {
      // Skip if there is already a value in this cell
      if (tectonic.cells[cellId].value) {
        return;
      }

      let nPossibleValues = block.valuesLeft.length;
      let possibleValue = -1;
      // Try to match the values that are left
      block.valuesLeft.forEach((value) => {
        if (
          tectonic.cells[cellId].neighbors.some(
            (neighborId) => tectonic.cells[neighborId].value === value,
          )
        ) {
          nPossibleValues--;
        } else {
          possibleValue = value;
        }
      });
      if (nPossibleValues === 1 && possibleValue !== -1) {
        answers.push({ cellId, value: possibleValue });
      }
    });
  });

  if (answers.length !== 0) {
    return batchFill(tectonic, answers, options);
  }

  /**
   * LEVEL 3
   */
  if (tectonic.level < 3) {
    tectonic.level = 3;
  }

  // (3.0) Set options array for all empty cells
  if (!tectonic.trackOptions) {
    setOptions(tectonic);
    tectonic.trackOptions = true;
  }

  // (3.1) Check if all empty nonBlockNeighbors options combined make it impossible for a valueLeft in a block empty cellIds
  tectonic.cells
    .filter((cell) => cell.options.length > 0)
    .forEach((cell) => {
      // If these non block neighbors are all the empty values in that block
      const completeEmptyNonBlockNeighborIdsPerBlock = Object.entries(
        cell.nonBlockNeighborsPerBlock,
      )
        .filter(
          ([blockId, cellIds]) =>
            tectonic.blocks[Number(blockId)].valuesLeft.length === cellIds.length,
        )
        .map((entries) => entries[1]); // We don't need the blockId anymore, its enough that these neighborIds are grouped per block
      // Remove options for this cell if they appear in all 'complete' empty non block neighbor options
      if (completeEmptyNonBlockNeighborIdsPerBlock) {
        // Per neighboring block (every neighboring block potentially can remove options for this cell)
        completeEmptyNonBlockNeighborIdsPerBlock.forEach((completeEmptyNonBlockNeighborIds) => {
          // Update the options of this cell
          const newOptions = cell.options.filter(
            (option) =>
              // It must be the answer if for non (!every) of the non block neigboring cells
              !completeEmptyNonBlockNeighborIds.every((neighborId) =>
                // The current option is also an option
                tectonic.cells[neighborId].options.some(
                  (neighborOption) => neighborOption === option,
                ),
              ),
          );
          if (newOptions.length === 1) {
            answers.push({ cellId: cell.cellId, value: newOptions[0] });
          }
        });
      }
    });
  if (answers.length !== 0) {
    return batchFill(tectonic, answers, options);
  }

  // (3.2) Check if an option only occurs in neighboring cells and not in any other cells in the
  // neighboring block, then that is not an option for this cell
  let optionsUpdated = false;
  tectonic.cells
    .filter((cell) => cell.options.length > 0)
    .forEach((cell) => {
      // Filter out the options that only occur in non-block neighboring cell options and not in any
      // of the non-neighboring cells in the neighboring block
      cell.options = cell.options.filter((option) => {
        const neighborBlocks = Object.entries(cell.nonBlockNeighborsPerBlock);

        // These neighboring blocks cannot help us here. Keep the option
        if (neighborBlocks.length === 0) {
          return true;
        }

        // Remove answered values from neigboring options
        neighborBlocks.forEach((neighborBlock) => {
          neighborBlock[1] = neighborBlock[1].filter(
            (neighborCellId) => tectonic.cells[neighborCellId].options.length > 0,
          );
        });

        // Keep this option if every subcondition is true
        return neighborBlocks.every(([blockId, neighborBlockNeighbors]) => {
          // This there are no empty cells left in this neighboring block, keep the option
          if (neighborBlockNeighbors.length === 0) {
            return true;
          }
          const optionOccursInNeighbors = neighborBlockNeighbors.some((neighborCellId) =>
            tectonic.cells[neighborCellId].options.some(
              (neighborOption) => neighborOption === option,
            ),
          );

          // This option does not occur in any of the neigbors, this rule can't help to exclude it
          if (!optionOccursInNeighbors) {
            return true;
          }
          // Get all other empty cells in this neigboring block
          const emptyNonNeighborsInNeighboringBlock = tectonic.blocks[
            Number(blockId) // Every cell in this neigboring block
          ].cellIds.filter(
            (cellId) =>
              tectonic.cells[cellId].options.length > 0 && // Cell is empty
              !neighborBlockNeighbors.some((neighborCellId) => neighborCellId === cellId), // Cell is not a neighbor
          );
          const optionOccursInRestOfBlock = emptyNonNeighborsInNeighboringBlock.some(
            (nonNeighborInNeighboringBlockCellId) =>
              tectonic.cells[nonNeighborInNeighboringBlockCellId].options.some(
                (nonNeigborOption) => nonNeigborOption === option,
              ),
          );

          if (optionOccursInRestOfBlock) {
            return true;
          }

          // Remove this option if it does not occur in the rest of the block
          // It must be used as a neigbor of this cell
          optionsUpdated = true;
          return false;
        });
      });
    });
  if (optionsUpdated) {
    return solver(tectonic, options);
  }

  // (3.1) Check if an option only occurs once in a block
  tectonic.blocks.forEach((block) => {
    const inBlockEmptyCellIds = block.cellIds.filter(
      (cellId) => tectonic.cells[cellId].options.length > 0,
    );
    inBlockEmptyCellIds.forEach((cellId) => {
      tectonic.cells[cellId].options.forEach((option) => {
        // Check if the option occurs in another empty cell in this block
        if (
          inBlockEmptyCellIds
            .filter((inBlockCellId) => inBlockCellId !== cellId)
            .every((inBlockCellId) =>
              tectonic.cells[inBlockCellId].options.every(
                (inBlockCellOption) => inBlockCellOption !== option,
              ),
            )
        ) {
          answers.push({ cellId, value: option });
        }
      });
    });
  });

  if (answers.length !== 0) {
    return batchFill(tectonic, answers, options);
  }

  /**
   * Level 6
   */
  if (tectonic.level < 6) {
    tectonic.level = 6;
  }

  // (6.1) If two non-block neighbors, who are neighbors (independent of block) of each other have
  // to be either one of two options each, they cancel out these two options in this block.
  tectonic.cells
    .filter((cell) => cell.options.length >= 3)
    .forEach((cell) => {
      const neigborWithTwoOptionsIds = cell.nonBlockNeighbors.filter(
        (neighborId) => tectonic.cells[neighborId].options.length === 2,
      );
      if (neigborWithTwoOptionsIds.length < 2) {
        return; // We need at least one pair of neighbors for this rule
      }
      const neigborSameOptionsIds = neigborWithTwoOptionsIds.filter((neighborId) =>
        tectonic.cells[neighborId].options.every((neighborOption) =>
          cell.options.some((option) => option === neighborOption),
        ),
      );
      if (neigborSameOptionsIds.length < 2) {
        return;
      }
      const neighborPairs: number[][] = [];
      neigborSameOptionsIds.forEach((firstId) => {
        neigborSameOptionsIds
          .filter((secondId) => secondId > firstId)
          .forEach((secondId) => {
            // Check if they are neighbors of each other
            if (!tectonic.cells[firstId].neighbors.some((neighborId) => neighborId === secondId)) {
              return;
            }
            // Check if they have the same options as each other
            if (
              !tectonic.cells[firstId].options.every((firstNeighborOption) =>
                tectonic.cells[secondId].options.some(
                  (secondNeighborOption) => secondNeighborOption === firstNeighborOption,
                ),
              )
            ) {
              return;
            }
            neighborPairs.push([firstId, secondId]);
          });
      });

      if (neighborPairs.length === 0) {
        return;
      }

      // There are two neighbors that are neighbors of each other, that both have two options which
      // are the same. These two options are no longer an option for this cell.
      neighborPairs.forEach((neighborPair) => {
        cell.options = cell.options.filter(
          (option) =>
            !tectonic.cells[neighborPair[0]].options.some(
              (neighborOption) => neighborOption === option,
            ),
        );
      });
      optionsUpdated = true;
    });

  if (optionsUpdated) {
    return solver(tectonic, options);
  }

  // If we got here we did not have any answers to fill,
  // and we don't have any more tricks up our sleeve, so it is unsolvable
  tectonic.isUnsolvable = true;
  tectonic.finish = performance.now();
  return tectonic;
};

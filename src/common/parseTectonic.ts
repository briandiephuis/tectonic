import { noWalls } from "../constants/tectonic";
import { getNeighbors } from "./getNeighbors";
import { getWalls } from "./tectonicUtils";

export const parseTectonic = (input: string): Tectonic => {
  // Sanitize input
  input = input
    .trim()
    .replace(/(\n|\s)/gm, "")
    .replace(/;$/g, "");

  const [inputSize, inputValues] = input.split(":");

  // Get size
  const [widthStr, heightStr] = inputSize.split("x");
  const size = {
    width: parseInt(widthStr),
    height: parseInt(heightStr),
  };
  let unsolvedCells = size.width * size.height;

  // Get the individual cell data
  const inputCells = inputValues.split(";");

  const cells: TectonicCell[] = [];
  const blocks: TectonicBlock[] = [];
  // Iteration 1: Parse basic values and get their neighbors
  for (let rowId = 0; rowId < size.height; rowId++) {
    for (let columnId = 0; columnId < size.width; columnId++) {
      const cellId = rowId * size.width + columnId;
      const [blockIdStr, valueStr] = inputCells[cellId].split("-");
      const blockId = Number(blockIdStr);
      const valueNaN = Number(valueStr);
      const value = isNaN(valueNaN) || valueNaN === 0 ? undefined : valueNaN;
      const isGiven = value !== undefined;
      if (isGiven) {
        unsolvedCells--;
      }
      const neighbors = getNeighbors(cellId, size, rowId, columnId);
      cells.push({
        cellId,
        blockId,
        columnId,
        rowId,
        value,
        isGiven,
        neighbors,
        options: [],
        blockNeighbors: [],
        nonBlockNeighbors: [],
        nonBlockNeighborsPerBlock: {},
        isError: false,
        walls: noWalls,
      });

      // Create block if it does not yet exist
      if (blocks[blockId]) {
        blocks[blockId].cellIds.push(cellId);
      } else {
        blocks[blockId] = {
          blockId,
          cellIds: [cellId],
          size: -1,
          valuesLeft: [],
        };
      }
    }
  }

  if (size.width * size.height !== inputCells.length) {
    throw new Error("Input values does not match Tectonic size");
  }

  // Iteration 2: Get the neighbors in the same block and the walls per cell
  cells.forEach((cell) => {
    cell.blockNeighbors = cell.neighbors.filter((neighborCellId) =>
      blocks[cell.blockId].cellIds.some((cellId) => cellId === neighborCellId),
    );
    cell.nonBlockNeighbors = cell.neighbors.filter(
      (neighborCellId) => !blocks[cell.blockId].cellIds.some((cellId) => cellId === neighborCellId),
    );
    cell.walls = getWalls(cell, size);
    cell.nonBlockNeighbors.forEach((neighborId) => {
      // Create a dictionary grouping the nonBlockNeighbors per block by blockId
      const { blockId } = cells[neighborId];
      if (cell.nonBlockNeighborsPerBlock[blockId]) {
        cell.nonBlockNeighborsPerBlock[blockId].push(neighborId);
      } else {
        cell.nonBlockNeighborsPerBlock[blockId] = [neighborId];
      }
    });
  });
  blocks.forEach((block) => {
    block.size = block.cellIds.length;
    const blockValues = block.cellIds.map((cellId) => cells[cellId].value);
    for (let i = 1; i < block.size + 1; i++) {
      if (!blockValues.some((id) => id === i)) {
        block.valuesLeft.push(i);
      }
    }
  });

  return {
    level: 1,
    size,
    blocks,
    cells,
    nUnsolvedCells: unsolvedCells,
    iteration: 0,
    isUnsolvable: false,
    trackOptions: false,
  };
};

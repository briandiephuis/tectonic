export const checkTectonicSimpleErrors = (tectonic: Tectonic): boolean => {
  const errorCellIds: Set<number> = new Set();

  // Check per block
  tectonic.blocks.forEach((block) => {
    const values: { cellId: number; value: number }[] = [];
    block.cellIds.forEach((cellId) => {
      const value = tectonic.cells[cellId].value;

      // Check for higher values than block size
      if (value && value > block.size) {
        errorCellIds.add(cellId);
      }

      // Log used value for comparison
      if (value) {
        values.push({ cellId, value });
      }
    });

    // Check for same values in this block (if there is more than 1 value)
    if (values.length > 1) {
      values.sort(({ value: a }, { value: b }) => a - b); // Sort by value
      let refIndex = 0;
      const sameValueCellIds: number[] = [];
      let index = refIndex + 1;
      while (index < values.length) {
        if (values[index].value === values[refIndex].value) {
          if (index === refIndex + 1) {
            sameValueCellIds.push(values[refIndex].cellId);
          }
          sameValueCellIds.push(values[index].cellId);
        } else {
          refIndex = index;
        }
        index++;
      }

      sameValueCellIds.forEach((cellId) => {
        errorCellIds.add(cellId);
      });
    }
  });

  // Check for same values as non-block neighbors
  const checkedCellIds: Set<number> = new Set();
  tectonic.cells
    .filter((cell) => cell.value)
    .forEach((cell) => {
      if (checkedCellIds.has(cell.cellId)) {
        return;
      }
      cell.nonBlockNeighbors
        .filter((neighborCellId) => checkedCellIds.has(neighborCellId))
        .forEach((neighborCellId) => {
          if (tectonic.cells[neighborCellId].value === cell.value) {
            checkedCellIds.add(neighborCellId);
            errorCellIds.add(cell.cellId);
            errorCellIds.add(neighborCellId);
          }
        });

      checkedCellIds.add(cell.cellId);
    });

  // Update cells
  let shouldUpdate = false;
  tectonic.cells.forEach((cell) => {
    const hasError = errorCellIds.has(cell.cellId);
    if (cell.isError !== hasError) {
      shouldUpdate = true;
    }
    cell.isError = hasError;
  });

  return shouldUpdate;
};

interface TectonicSize {
  width: number;
  height: number;
}

interface TectonicWalls {
  top: boolean;
  right: boolean;
  bottom: boolean;
  left: boolean;
}

interface TectonicBlock {
  blockId: number;
  cellIds: number[];
  size: number;
  valuesLeft: number[];
}

interface TectonicCell {
  cellId: number;
  rowId: number;
  columnId: number;
  blockId: number;
  value?: number;
  options: number[];
  isError?: boolean;
  isGiven: boolean;
  walls: TectonicWalls;
  neighbors: number[];
  blockNeighbors: number[];
  nonBlockNeighbors: number[];
  nonBlockNeighborsPerBlock: {
    [key: number]: number[];
  };
}

interface Tectonic {
  level: number;
  size: TectonicSize;
  blocks: TectonicBlock[];
  cells: TectonicCell[];
  nUnsolvedCells: number;
  iteration: number;
  isUnsolvable: boolean;
  trackOptions: boolean;
  start?: number;
  finish?: number;
}

interface TectonicAnswer {
  cellId: number;
  value: number;
}

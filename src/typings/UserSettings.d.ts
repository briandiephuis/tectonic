interface UserSettings {
  isDrawerOpen: boolean;
  darkMode?: boolean;
}

import { createStyles, makeStyles, Theme } from "@material-ui/core";
import React from "react";

import { parseTectonic } from "../../common/parseTectonic";
import Tectonic from "../../components/Tectonic/Tectonic";
import { level6 } from "../../puzzles/level6";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      margin: "0 auto",
      padding: theme.spacing(2),
    },
  }),
);

const tectonic = parseTectonic(level6[0]);

const TectonicPage: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Tectonic puzzleNumber={1} tectonic={tectonic} />
    </div>
  );
};

export default TectonicPage;

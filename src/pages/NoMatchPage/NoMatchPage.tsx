import { Button, createStyles, makeStyles, Theme, Typography } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      margin: "0 auto",
      padding: theme.spacing(2),
    },
  }),
);

const NoMatchPage: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography gutterBottom variant="h4">
        404 - Page not found
      </Typography>
      <Link color="primary" component={Button} to="/">
        Return home
      </Link>
    </div>
  );
};

export default NoMatchPage;

import { createStyles, makeStyles, Theme } from "@material-ui/core";
import React from "react";

import CreateTectonic from "../../components/CreateTectonic/CreateTectonic";
import { newTectonic } from "../../constants/tectonic";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      margin: "0 auto",
      padding: theme.spacing(2),
    },
  }),
);

const CreateTectonicPage: React.FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CreateTectonic edit puzzleNumber={1} tectonic={newTectonic} />
    </div>
  );
};

export default CreateTectonicPage;

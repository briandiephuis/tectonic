export const newTectonic: Tectonic = {
  blocks: [],
  cells: [],
  size: {
    width: 4,
    height: 5,
  },
  isUnsolvable: false,
  level: 1,
  nUnsolvedCells: -1,
  iteration: -1,
  trackOptions: false,
};

export const noWalls: TectonicWalls = {
  top: false,
  right: false,
  bottom: false,
  left: false,
};

export const tectonicMaxBlockSize = 5;

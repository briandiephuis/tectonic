import { createMuiTheme, CssBaseline, MuiThemeProvider, useMediaQuery } from "@material-ui/core";
import React, { useMemo, useState } from "react";
import { hot } from "react-hot-loader/root";
import { BrowserRouter as Router, Route, RouteProps, Switch } from "react-router-dom";

import { initialUserSettings, UserSettings } from "./common/UserSettings";
import Drawer from "./components/Drawer/Drawer";
import CreateTectonicPage from "./pages/CreateTectonicPage/CreateTectonicPage";
import NoMatchPage from "./pages/NoMatchPage/NoMatchPage";
import TectonicPage from "./pages/TectonicPage/TectonicPage";

/**
 * Theme:
 * #F279B2 (pink)
 * #465EA6 (blue)
 * #D9D8D2 (gray)
 * #F2BD1D (yellow)
 * #F25757 (red)
 */

const routes: RouteProps[] = [
  { path: "/", component: TectonicPage, exact: true },
  { path: "/create", component: CreateTectonicPage },
];

const App = (): any => {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
  const [userSettings, setUserSettings] = useState(initialUserSettings);

  const isDarkMode = useMemo(
    () => (userSettings.darkMode !== undefined ? userSettings.darkMode : prefersDarkMode),
    [prefersDarkMode, userSettings.darkMode],
  );

  const theme = useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: isDarkMode ? "dark" : "light",
          primary: {
            main: "#465EA6",
          },
          secondary: {
            main: isDarkMode ? "#947E12" : "#F2BD1D",
          },
          background: {
            default: isDarkMode ? "#2B2B2A" : "#D9D8D2",
          },
          error: {
            main: "#F25757",
          },
          text: {
            primary: isDarkMode ? "#BBB" : "#333",
            secondary: isDarkMode ? "#777" : "#222",
          },
        },
      }),
    [isDarkMode],
  );

  return (
    <UserSettings.Provider value={[userSettings, setUserSettings]}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Router>
          <Drawer />
          <Switch>
            {routes.map((route) => (
              <Route key={(route.path || "").toString()} {...route} />
            ))}
            <NoMatchPage />
          </Switch>
        </Router>
      </MuiThemeProvider>
    </UserSettings.Provider>
  );
};

// export default App;
export default process.env.NODE_ENV === "development" ? hot(App) : App;

/* eslint-disable */
const rewireReactHotLoader = require("react-app-rewire-hot-loader");

/* config-overrides.js */
module.exports = {
  webpack: function (config, env) {
    if (env == "development") {
      config.resolve.alias["react-dom"] = "@hot-loader/react-dom";
    }
    config = rewireReactHotLoader(config, env);
    return config;
  },
  devServer: function (configFunction) {
    // Return the replacement function for create-react-app to use to generate the Webpack
    // Development Server config. "configFunction" is the function that would normally have
    // been used to generate the Webpack Development server config - you can use it to create
    // a starting configuration to then modify instead of having to create a config from scratch.
    return function (proxy, allowedHost) {
      // Create the default config by calling configFunction with the proxy/allowedHost parameters
      const config = configFunction(proxy, allowedHost);

      config.proxy = {
        "/graphql": {
          target: "http://api:8000",
          secure: false,
          logLevel: "debug",
        },
      };

      // Return your customized Webpack Development Server config.
      return config;
    };
  },
};
